#!/bin/bash

# Open configure files
services='services.txt'
try_icons='try_icons.txt'
apps='apps.txt'

# Stop unnecessary services
while read service; do
    systemctl stop $service
done < $services

# kill applets and try icons
while read try_icon; do
    killall $try_icon
done < $try_icons

# Kill apps
while read app; do
    killall $app
done < $apps

# Drop memory caches
sync; echo 1 > /proc/sys/vm/drop_caches
